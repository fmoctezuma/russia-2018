FROM node:10.0.0
#Also tested on
#FROM node:6.11.4
#FROM node:8.11.2

EXPOSE 4200 7020

RUN git clone https://gitlab.com/fmoctezuma/russia-2018.git /russia-2018

WORKDIR /russia-2018

# Install watchman build dependencies
RUN \
        apt-get update -y &&\
        apt-get install -y python-dev

#If it's desired to launch ember with self signed certs(https) (Doesn't apply for k8s)
#RUN \
	#     mkdir /russia-2018/tls && chmod +x ./utils/gencert.sh &&\
	#./utils/gencert.sh

# install watchman
# Note: See the README.md to find out how to increase the
# fs.inotify.max_user_watches value so that watchman will
# work better with ember projects.
RUN \
        git clone https://github.com/facebook/watchman.git &&\
        cd watchman &&\
        git checkout v4.7.0 &&\
        ./autogen.sh &&\
        ./configure &&\
        make &&\
        make install

# install yarn
RUN \
    apt-get update && \
    apt-get install -y curl apt-transport-https && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && \
    apt-get install -y yarn

# install bower
RUN \
        npm install -g bower@1.8.0
# install official phantomjs binaries
RUN \
	mkdir /tmp/phantomjs &&\
	curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 | tar -xvj -C /tmp/phantomjs --strip-components=1 phantomjs-2.1.1-linux-x86_64/bin &&\
	mv /tmp/phantomjs/bin/phantomjs /usr/bin &&\
	rm -rf /tmp/phantomjs

# terminal sessions
RUN \
        echo 'PS1="\[\\e[0;94m\]${debian_chroot:+($debian_chroot)}\\u@\\h:\\w\\\\$\[\\e[m\] "' >> ~/.bashrc

RUN \
	npm install -g ember-cli@3.1.4
RUN \
	npm install
# run ember server on container start
CMD ["ember", "server"]