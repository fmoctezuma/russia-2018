require 'rubygems'
require 'pp'
require "net/http"
require 'json'
require 'firebase'

#########
# Utils
#########

def getWinner(home, visitor)
  if home > visitor
    winner = 'home'
  elsif home < visitor
    winner = 'visitor'
  else
    winner = 'tied'
  end
end

def getScore(home, visitor, homePrediction, visitorPrediction)
  points = 0

  if home >= 0 && !homePrediction.nil? && !visitorPrediction.nil?
    if home == homePrediction && visitor == visitorPrediction
      points = 15;
    elsif getWinner(home, visitor) == getWinner(homePrediction, visitorPrediction)
      points = 10 - (homePrediction - home).abs - (visitorPrediction - visitor).abs
      if points < 0
        points = 0
      end
    else
      points = 0
    end
  end

  if points.nil?
    points = 0
  end

  points

end

#-------
# Main
#-------
results = {}

team = {}
team["Russia"] = "RUS"
team["Saudi Arabia"] = "KSA"
team["Egypt"] = "EGY"
team["Uruguay"] = "URU"
team["Morocco"] = "MAR"
team["Iran"] = "IRN"
team["Portugal"] = "POR"
team["Spain"] = "ESP"
team["France"] = "FRA"
team["Australia"] = "AUS"
team["Argentina"] = "ARG"
team["Iceland"] = "ISL"
team["Peru"] = "PER"
team["Denmark"] = "DEN"
team["Croatia"] = "CRO"
team["Nigeria"] = "NGA"
team["Costa Rica"] = "CRC"
team["Serbia"] = "SRB"
team["Germany"] = "GER"
team["Mexico"] = "MEX"
team["Brazil"] = "BRA"
team["Switzerland"] = "SUI"
team["Sweden"] = "SWE"
team["Korea Republic"] = "KOR"
team["Belgium"] = "BEL"
team["Panama"] = "PAN"
team["Tunisia"] = "TUN"
team["England"] = "ENG"
team["Colombia"] = "COL"
team["Japan"] = "JPN"
team["Poland"] = "POL"
team["Senegal"] = "SEN"

agent = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"
key = "YOUR KEY"
server = "api.football-data.org"
path = "/v1/competitions/467/fixtures"

#-----------------------------------------
puts " Get scores from Football Data API "
#-----------------------------------------
http = Net::HTTP.new(server, 80)
req = Net::HTTP::Get.new(path, {'User-Agent' => agent, 'X-Auth-Token' => key})
response = http.request(req)
json = JSON.parse(response.body)

json['fixtures'].each do |e|
  if e['homeTeamName'] != '' 
    k = "#{team[e['homeTeamName']].downcase}_#{team[e['awayTeamName']].downcase}" 
    results[k] = {}
    results[k]["home"] = e['result']['goalsHomeTeam']
    results[k]["visitor"] = e['result']['goalsAwayTeam']
  end 
end 


#----------------------
# Connect to firebase
#----------------------
fb_config = {}
fb_config["databaseURL"] =  "YOUR FIREBASE URL"
fb_config["privateKey"]  = "YOUR PRIVATE KEY"

private_key_json_string = File.open(fb_config["privateKey"]).read
firebase = Firebase::Client.new(fb_config["databaseURL"], private_key_json_string)


#-----------------------
# Update matches scores 
#-----------------------

matches = {}
matchesFB = firebase.get('matches')

#------------------------------
puts " Create matches array..."
#------------------------------
matchesFB.body.each_with_index do |m, index|
  if !m.nil?
    key = "#{m['home']}_#{m['visitor']}"

    m['homeReal'] = results[key]['home']
    m['visitorReal'] = results[key]['visitor']

    matches[index] = m
  end
end

#--------------------------
puts " Update matches..."
#--------------------------
matches.each do |k, m|
  if !m['homeReal'].nil? &&
     !m['visitorReal'].nil? &&
     (m['homeReal'].to_i != m['homeGoals'].to_i ||
      m['visitorReal'].to_i != m['visitorGoals'].to_i)

   puts " Update #{m['home']} vs. #{m['visitor']} from #{m['homeGoals']}-#{m['visitorGoals']} to #{m['homeReal']}-#{m['visitorReal']}"

   data = { :homeGoals => m['homeReal'], :visitorGoals => m['visitorReal'] }

   response = firebase.update("matches/#{k}", data)
   if response.success?
     puts "OK"
   else
     puts "FAIL"
   end


  end
end

#-------------------------
puts " Get user scores..."
#-------------------------
users = firebase.get('users')
user_scores = {}
users.body.each do |key,val|
  user_scores[key] = val['score']
end


#--------------------------------
puts " Calculate users scores..."
#--------------------------------
scores = {}
predictions = firebase.get('predictions')

predictions.body.each do |key,val|
  aux = key.split('_')
  match_id = aux.pop.to_i
  user_id = aux.join('_')

  # Calculate if match has goals
  if !matches[match_id]['homeReal'].nil?
    s = getScore(matches[match_id]['homeReal'], matches[match_id]['visitorReal'], val['homePrediction'], val['visitorPrediction'])
    if scores[user_id].nil?
      scores[user_id] = s
    else
      scores[user_id] += s
    end
  end
  

end

#---------------------
puts " Update users..."
#---------------------
scores.each do |user,score|
  if user_scores[user] != score
    puts " Update '#{user}' score to #{score}"
    response = firebase.update("users/#{user}", {:score => score})
    if response.success?
      puts "OK"
    else
      puts "FAIL"
    end
  else
    puts " User #{user} score up to date"
  end
end

