#!/bin/bash
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out $PWD/tls/tls.key
rm server.pass.key
openssl req -new -key $PWD/tls/tls.key -out $PWD/tls/tls.csr \
	    -subj "/C=US/ST=Texas/L=SAT/O=example/OU=IT/CN=example.org"
openssl x509 -req -days 365 -in $PWD/tls/tls.csr -signkey $PWD/tls/tls.key -out $PWD/tls/tls.crt