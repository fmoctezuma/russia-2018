import DS from 'ember-data';
import { computed } from '@ember/object';


export default DS.Model.extend({
  date:         DS.attr('string'),
  home:         DS.belongsTo('team', { async: true }),
  homeGoals:    DS.attr('number'),
  visitor:      DS.belongsTo('team', { async: true }),
  visitorGoals: DS.attr('number'),
  
  matchDate: computed('id', function() {
   var date = new Date(this.get('date'));
   return date.toString().substring(0,10);
  }),

  matchTime: computed('id', function() {
   var date = new Date(this.get('date'));
   return date.toString().substring(15,21);
  }),

});
