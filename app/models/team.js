import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  name:  DS.attr('string'),
  group: DS.attr('string'),

  flag: computed('id', function() {
    return `/assets/images/${this.get('id')}.png`;
  })
});
