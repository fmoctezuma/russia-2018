import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { getOrdinal } from '../utils/scoreUtils';
import { htmlSafe } from '@ember/string';

export default Controller.extend({

  sortedUsers: computed('model.length', 'sortedUsers.@each.score', function() {
    return this.get('model').sortBy('score').reverse();
  }),

  highScores: computed('sortedUsers.@each.score', function() {
    var pos = 0;
    var previous = 0;
    return this.get('sortedUsers').map(function(i, idx) {
      if (i.get('score') != previous) {
        pos += 1;
        previous = i.get('score');
      }
      i.set('position', pos);
      i.set('positionOrdinal', htmlSafe(getOrdinal(pos)));
      i.set('positionClass', 'position-' + i['position']);
      return i;
    });
  })
});
