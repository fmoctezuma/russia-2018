### Kubernetes Deployment


Deploy ingress controller
```
helm install stable/nginx-ingress --namespace kube-system
```

Deploy Kube lego to use let's encrypt
```
helm install stable/kube-lego --namespace kube-system --set config.LEGO_EMAIL=YOUR_EMAIL,config.LEGO_URL=https://acme-v01.api.letsencrypt.org/directory
```

**Configure manifests on this directory according your case and deploy it to k8s**

Gotchas
- See Dockerfile for specific user case, `.embercli` is not running using tls, the SSL part is provided by the ingress controller
- livereload not exported/configured to be use on the ingress controller, after several trial/error test couldn't make it SSL passtrough from ingress 
SSL valid certs to autogent certs on the container.
- Firebase credentials are consumed using k8s secrets see secrets file for example.
